<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'test_wp');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UlozHsO;FoUgzQS D>rG`j+sdvJ/~dl)/+cSG6F9S_E{Nhdf{vB{+3v9X`I?Y!R:');
define('SECURE_AUTH_KEY',  'P]_A-<^wBkIWrN/@x|@wg$sI(IK(R,<[(3+6ag(;x6k77`.fVNL`&w0~}=w{8`Z$');
define('LOGGED_IN_KEY',    ',]7|pR7~|}@f8ThE rLW)Yti^gi~`$FM!F#q5@m4#I[@UEq48O-1t[h75 S.B9Jw');
define('NONCE_KEY',        'tCju>}w1fQ(4LL_,1{?5$5|%$K.%Y.:@$CAR14I*@R2RS|KZfgwJW`vKug2]!U5}');
define('AUTH_SALT',        'R.bh+3eoTBlF@O{a$D-r!@sB3j*Ta?  }$1<Yq7dgz.5<6w--z[pv&Tz)s:#rol*');
define('SECURE_AUTH_SALT', '|&LMi,AV|cjp3Vy=Ur:2L3C#Rx.%EKem+qwm/U{HZ_U%GFE.=r-x&N |u]+K%,$y');
define('LOGGED_IN_SALT',   '1!5>QNAU/-$Jfp8hR}%UGCk3ZHzih,PX[.r(.V,6r,XZnQ+YN/++vk#r$(JAO5EA');
define('NONCE_SALT',       'wIkXe)_r|M7O 38*le=>+lgE18lm^Lq-0_XY?p&R*vGr?7$!~|Decmc_Z_pWCfR%');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
