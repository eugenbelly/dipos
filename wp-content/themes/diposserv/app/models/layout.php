<?php
require('wp-content/themes/diposserv/app/functions.php');

$items = wp_get_nav_menu_items( 'Top', [

    'order'                  => 'ASC',
    'orderby'                => 'menu_order',
    'post_type'              => 'nav_menu_item',
    'post_status'            => 'publish',
    'output'                 => ARRAY_A,
    'output_key'             => 'menu_order',
    'nopaging'               => true,
    'update_post_term_cache' => false
]);

function currentPageSubUrl($url){
    return explode("/", $url)[3];
}

function currentPageIsActive($url){
    if(explode("/", $_SERVER['REQUEST_URI'])[1] === currentPageSubUrl($url)){
        return 'active';
    } else {
        return '';
    }
}

function menuMappingCallback($item){
    return [
        'id' => intval($item->ID),
        'parent_id' => intval($item->menu_item_parent),
        'suburl' => get_post($item->object_id)->post_content ? currentPageSubUrl($item->url) : "#",
        'title' => $item->title,
        'isActive' => currentPageIsActive($item->url)
    ];
}

function updateUpperMenuOfActive(&$tree){
    foreach ($tree as &$btn) {
        if(isset($btn['nodes'])) {
            $nodes = $btn['nodes'];
            foreach ($nodes as $node) {
                if ($node['isActive'] === 'active') {
                    $btn['isActive'] = 'active';
                    break; //Optimization
                }
            }
            if ($btn['isActive'] === 'active') {
                break; //Optimization
            }
        }
    }
}

$map = array_map("menuMappingCallback", $items);
$tree = array2tree($map, 0, 'nodes');

updateUpperMenuOfActive($tree);

//print_r(get_page_by_path('gallery'));


$images = [];

$gallery = get_post_gallery( get_page_by_path('gallery')->ID, false );

foreach(explode(',', $gallery['ids']) as $id) {

    $fullImgSrc = wp_get_attachment_image_src($id, 'full')[0];
    $fullImgWidth = wp_get_attachment_image_src($id, 'full')[1];
    $fullImgHeight = wp_get_attachment_image_src($id, 'full')[2];

    $thimbImgSrc = wp_get_attachment_image_src($id, 'thumbnail')[0];
    $thimbImgWidth = wp_get_attachment_image_src($id, 'thumbnail')[1];
    $thimbImgHeight = wp_get_attachment_image_src($id, 'thumbnail')[2];

//    print_r(get_post($id)->post_excerpt);

    $images[] = [
        'full' => [
            'src' => $fullImgSrc,
            'size' => $fullImgWidth.'x'.$fullImgHeight,
        ],

        'thumb' => [
            'src' => $thimbImgSrc,
            'size' => $thimbImgWidth.'x'.$thimbImgHeight,
        ],

        'caption' => get_post($id)->post_excerpt
    ];

}

return [
    'menu' => $tree,
    'content' => get_post(get_the_ID())->post_content,
    'images' => $images,
];