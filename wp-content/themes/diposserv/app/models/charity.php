<?php

$request = $_SERVER['REQUEST_URI'];
$category = explode("/", $_SERVER['REQUEST_URI'])[1];

$args = array(
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => $category,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'post',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
);


function map($element){
    $element->post_content = str_split($element->post_content, 100)[0];
    return $element;
}

$prepared = array_map('map', get_posts( $args ));

print_r($prepared);
return [
    'articles' => $prepared
];