<?php

$images = [];

$gallery = get_post_gallery( get_the_ID(), false );

foreach(explode(',', $gallery['ids']) as $id) {

    $fullImgSrc = wp_get_attachment_image_src($id, 'full')[0];
    $fullImgWidth = wp_get_attachment_image_src($id, 'full')[1];
    $fullImgHeight = wp_get_attachment_image_src($id, 'full')[2];

    $thimbImgSrc = wp_get_attachment_image_src($id, 'thumbnail')[0];
    $thimbImgWidth = wp_get_attachment_image_src($id, 'thumbnail')[1];
    $thimbImgHeight = wp_get_attachment_image_src($id, 'thumbnail')[2];

//    print_r(get_post($id)->post_excerpt);

    $images[] = [
        'full' => [
            'src' => $fullImgSrc,
            'size' => $fullImgWidth.'x'.$fullImgHeight,
        ],

        'thumb' => [
            'src' => $thimbImgSrc,
            'size' => $thimbImgWidth.'x'.$thimbImgHeight,
        ],

        'caption' => get_post($id)->post_excerpt
    ];

}

return [
    'images' => $images
];