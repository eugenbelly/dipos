<?php
function prepareTwig(){

    function getHtml($name){
        return file_get_contents("wp-content/themes/diposserv/app/views/$name.twig");
    }

    $views = [];

    foreach (scandir('wp-content/themes/diposserv/app/views') as $entity) {
        if( $entity !== '.' && $entity !== '..') {
            $entity = explode(".", $entity)[0];
            $views[$entity] = getHtml($entity);
        }
    }

    return new Twig_Environment(new Twig_Loader_Array($views));
}

function getCurrentPageSlug(){
    return explode("/", $_SERVER['REQUEST_URI'])[1];
}

function route(){
    $uniquePages = require('app/routing.php');

    $slug = getCurrentPageSlug();

    if(in_array($slug, $uniquePages)){
        return $slug;
    } else {
        return 'layout';
    }
}

function getVariables($templates){
    $resut = [];
    foreach($templates as $template){
        $resut = array_merge(
            $resut,
            require('app/models/'.$template.'.php')
        );
    }
    return $resut;
}

function getNecessaryTplVars(){
    if(route() === 'layout') {
        return getVariables([
            'layout'
        ]);
    } else {
        return getVariables([
            'layout',
            getCurrentPageSlug()
        ]);
    }
}

echo prepareTwig()->render(route(), getNecessaryTplVars());
